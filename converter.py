import PySimpleGUI as sg
import numpy as np
import types

class converter:
	def __init__(self):
		self.units_old = []
		self.units_new = []
		self.func_convs = []
	
	def run(self):
		"""
		display all basic gui elements
		and stay ready for loading modules via self.load
		"""
		
		row1 = [sg.Text("Select input unit:"), sg.Spin(self.units_old,key="-SELECTION-")]
		row2 = [sg.Text("Enter value in selected unit:"), sg.Input(key="-INPUT-")]
		row3 = [sg.Button("Convert", key="-CONVERT-")]
		row4 = [sg.Text("", key = "-RESULT-")]
		layout = [row1,row2,row3,row4]
		window = sg.Window("Converter", layout)
		while True:
			event, values = window.read()
			result = ""
			result_display = ""
			if event == sg.WIN_CLOSED:
				break
			if event == "-CONVERT-":
				if values["-INPUT-"].isnumeric():
					try:
						index_unit_old = self.units_old.index(values["-SELECTION-"])
					except:
						print("Error, unit not found")
					result = self.func_convs[index_unit_old](float(values["-INPUT-"]))
					result_display = f"The result is: {result} {self.units_new[index_unit_old]}"
					window["-RESULT-"].update(result_display)
				else:
					window["-RESULT-"].update("Error, only numeric input accepted")
					
		window.close()

	def load(self, units, func_conv):
		"""
		add options in drop down
		and issue func_conv if corresponding
		units dictionary {unit_old: unit_new} is selected there
		"""
		if type(func_conv) is types.FunctionType:
			loop_range = 1
			func_conv = [func_conv]
		elif type(func_conv) is list:
			loop_range = len(func_conv)
		else:
			print("Error, function variable needs to be a function or list of functions")
		keys = [key for key in units.keys()]
		values= [val for val in units.values()]
		for i in range(loop_range):
			self.units_old.append(keys[i])
			self.units_new.append(values[i])
			self.func_convs.append(func_conv[i])


if __name__ == "__main__":
	units_length = {"Meters":"Feet"}
	units_temperature = {"Celsius":"Fahrenheit"}
	units_length2= {"inch":"cm"}

	def convert_meters_to_feet(meters):
		return meters/ 0.3048

	def convert_celsius_to_fahrenheit(celsius):
		return 9*celsius/5 + 32

	def convert_inch_to_cm(inch):
		return inch*2.54

	conv = converter()
	conv.load(units_length, convert_meters_to_feet)
	conv.load(units_temperature, convert_celsius_to_fahrenheit)
	conv.load(units_length2, convert_inch_to_cm)
	conv.run()
